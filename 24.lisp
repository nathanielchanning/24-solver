;;;; Solves 
(defvar *base-ops* '(+ - * /))

(defun gen-full-ops ()
  (let ((acc nil))
    (loop :for i :in *base-ops*
          :do (loop :for j :in *base-ops*
                    :if (not (eq i j))
                    :do (loop :for k :in *base-ops*
                              :if (not (or (eq i k) (eq j k)))
                              :do (setf acc (append acc (list (list i j k)))))))
    acc))

(defun calculate (ops nums)
  (loop :for (op1 op2 op3) :in (list ops)
        :for (num1 num2 num3 num4) :in (list nums)
        :return (funcall op1 num1 (funcall op2 num2 (funcall op3 num3 num4)))))

;;; http://stackoverflow.com/questions/2087693/how-can-i-get-all-possible-permutations-of-a-list-with-common-lisp
(defun num-permutations (list &optional (remain list))
  (cond
    ((null remain) nil)
    ((null (cdr list)) (list list))
    (t (append
        (mapcar (lambda (l) (cons (first list) l))
                (num-permutations (rest list)))
        (num-permutations (append (rest list) (list (first list))) (rest remain))))))

(defun find-solutions (nums)
  (remove nil
          (let ((num-perms (num-permutations nums))
                (op-perms (gen-full-ops)))
            (loop :for num-perm :in num-perms
                  :append(loop :for op-perm :in op-perms
                               :if (eq 24 (handler-case (calculate op-perm num-perm)
                                      (division-by-zero () nil)))
                               :collect (append num-perm op-perm))))))

(defun print-solutions (nums)
  (loop :for (first second third fourth first-op second-op third-op) :in (find-solutions nums)
        :do (format t "(~a ~a (~a ~a (~a ~a ~a)))~%"
                    first-op first second-op second third-op third fourth)))
